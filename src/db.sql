DROP DATABASE IF EXISTS lez_19_rubrica;
CREATE DATABASE lez_19_rubrica;
USE lez_19_rubrica;

CREATE TABLE persona(
	PersonaID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Nome VARCHAR(250) NOT NULL,
    Cognome VARCHAR(250) NOT NULL,
    Email VARCHAR(250) NOT NULL,
    Telefono VARCHAR(250) NOT NULL
);

INSERT INTO persona(Nome, Cognome, Email, Telefono) VALUES
("Mario", "Rossi", "ciao@ciao.com", "123456"),
("Vittorio", "Verdi", "ciao@ciao.com", "78932"),
("Maria", "Azzurra", "ciao@ciao.com", "3498769");

SELECT * FROM persona;
DELETE FROM persona WHERE PersonaID = 14;

UPDATE persona SET nome = "Undi" WHERE PersonaID = 11;


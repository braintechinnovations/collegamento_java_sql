package com.lezione22.hw;

public class Cliente {

	private int ClienteID;
	private String nome;
    private String cognome;
    private String numeroTelefono;
    private String codiceCliente;
    
    Cliente(){
    	
    }

    Cliente(String varNome, String varCognome, String varNumero, String varCodice){
        this.nome = varNome;
        this.cognome = varCognome;
        this.numeroTelefono = varNumero;
        this.codiceCliente = varCodice;
    }
    
    public int getClienteID() {
		return ClienteID;
	}

	public void setClienteID(int clienteID) {
		ClienteID = clienteID;
	}

	public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public String getNumeroTelefono() {
        return numeroTelefono;
    }
    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }
    public String getCodiceCliente() {
        return codiceCliente;
    }
    public void setCodiceCliente(String codiceCliente) {
        this.codiceCliente = codiceCliente;
    }

	@Override
	public String toString() {
		return "Cliente [ClienteID=" + ClienteID + ", nome=" + nome + ", cognome=" + cognome + ", numeroTelefono="
				+ numeroTelefono + ", codiceCliente=" + codiceCliente + "]";
	}
	
	public String stringaCSV() {
		return this.nome + "," + this.cognome + "," + this.numeroTelefono + "," + this.codiceCliente + "\n";
	}

    
	
	
}

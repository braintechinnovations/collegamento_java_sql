package com.lezione22.hw;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class GestoreClienti {

	private ArrayList<Cliente> elenco = new ArrayList<Cliente>();
	private String basePath = System.getProperty("user.home") + "\\Desktop\\gestione_clienti";
	private String nomeFile = "estrapolazione.csv";
	
	GestoreClienti(){
		
	}
	
	/**
	 * PUNTO 1
	 * Effettuo la query di select generale su DB ed effettuo il riempimento del ArrayList elenco.
	 */
	public void leggiClientiDaDb() {
		
		elenco.clear(); 			//Svuoto l'elenco con i vecchi dati prima di inserire i nuovi!

		try {
			Connection conn = ConnettoreDB.getIstanza().getConnessione();
			
			String query = "SELECT id_cliente, nome, cognome, numero_tel, codice_cli FROM cliente";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
			
			ResultSet rs;
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Cliente cliTemp = new Cliente();
				cliTemp.setClienteID(rs.getInt(1));
				cliTemp.setNome(rs.getString(2));
				cliTemp.setCognome(rs.getString(3));
				cliTemp.setNumeroTelefono(rs.getString(4));
				cliTemp.setCodiceCliente(rs.getString(5));
				
				this.elenco.add(cliTemp);
			}
		} catch (SQLException e) {
			System.out.println("ERRORE DI CONNESSIONE");
			e.printStackTrace();
		}
	}
	
	public void stampaClientiInConsole() {
		for(int i=0; i<this.elenco.size(); i++) {
			Cliente temp = this.elenco.get(i);
			System.out.println(temp.toString());
		}
	}
	
	/**
	 * Inserisce un nuovo cliente ed assegna (tra poco) il codice cliente in maniera automatica!
	 * @param varNome
	 * @param varCognome
	 * @param varTelefono
	 * @return
	 */
	public boolean inserimentoCliente(String varNome, String varCognome, String varTelefono) {
		
		Cliente temp = new Cliente();
		temp.setNome(varNome);
		temp.setCognome(varCognome);
		temp.setNumeroTelefono(varTelefono);
		
		this.insert(temp);
		
		if(temp.getClienteID() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private boolean insert(Cliente objCliente) {
		try {
			Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
			String query;
			if(objCliente.getCodiceCliente() != null) {
				query = "INSERT INTO cliente (nome, cognome, numero_tel, codice_cli) VALUE (?,?,?,?)";
			}
			else {
				query = "INSERT INTO cliente (nome, cognome, numero_tel) VALUE (?,?,?)";
			}
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, objCliente.getNome());
			ps.setString(2, objCliente.getCognome());
			ps.setString(3, objCliente.getNumeroTelefono());
			if(objCliente.getCodiceCliente() != null)
				ps.setString(4, objCliente.getCodiceCliente());


			if(ps.executeUpdate() > 0) {
				ResultSet rs = ps.getGeneratedKeys();
				rs.next();
				
				int idGenerato = rs.getInt(1);
				if(idGenerato > 0) {
					objCliente.setClienteID(idGenerato);
					if(objCliente.getCodiceCliente() == null)
						objCliente.setCodiceCliente("CLI" + String.format("%03d", idGenerato));
					
					if(this.updateUtente(objCliente)) {
						return true;
					}
					else {
						//TODO: ci avete pensato?
						return false;
					}
				}
				else
					return false;
			}
			else {
				return false;
			}

		} catch (SQLException e) {
			System.out.println("ERRORE DI CONNESSIONE");
			return false;
		}
	}
	
	/**
	 * Funzione di update del cliente interna
	 * @param objCliente
	 * @return
	 */
	private boolean updateUtente(Cliente objCliente) {
		Connection conn;
		try {
			conn = ConnettoreDB.getIstanza().getConnessione();
			
			String query = "UPDATE cliente SET nome = ?, cognome = ?, numero_tel = ?, codice_cli = ? WHERE id_cliente = ?";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, objCliente.getNome());
			ps.setString(2, objCliente.getCognome());
			ps.setString(3, objCliente.getNumeroTelefono());
			ps.setString(4, objCliente.getCodiceCliente());
			ps.setInt(5, objCliente.getClienteID());
			
			int risultato = ps.executeUpdate();
			if(risultato > 0) {
				return true;
			}
			
			return false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}	
	}
	
	public void esportaDbInCsv() {
		this.leggiClientiDaDb();    //Leggo dal DB l'ultima versione della tabella aggiornata
		
		File d = new File(basePath);
		if(d.exists() && d.isDirectory() && d.canWrite()) {
			File f = new File(basePath + File.separator + nomeFile);
			
			try {
				f.createNewFile();
				
				FileWriter fw = new FileWriter(f, false);
				
				for(int i=0; i<this.elenco.size(); i++) {
					fw.write(this.elenco.get(i).stringaCSV());
				}
				
				fw.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		else {
			System.out.println("Errore nella cartella");
		}
	}
	
	public void importaInDbDaCsv() {
		File f = new File(basePath + File.separator + nomeFile);
		if(f.exists() && f.isFile() && f.canRead()) {
			
			try {
				Scanner scan = new Scanner(f);

				while(scan.hasNext()) {
					String riga = scan.nextLine().trim();
					
					if(!riga.isEmpty()) {
						String[] campi = riga.split(",");
						
						Cliente cli_temp = new Cliente();
						cli_temp.setNome(campi[0]);
						cli_temp.setCognome(campi[1]);
						cli_temp.setNumeroTelefono(campi[2]);
						if(campi.length == 4)
							cli_temp.setCodiceCliente(campi[3]);
						
						if(this.insert(cli_temp))
							System.out.println("Ho inserito il cliente " + cli_temp.toString());
						else 
							System.out.println("Cliente non inserito");
					}
					
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
		}
		else {
			System.out.println("Errore nel file");
		}
	}
}

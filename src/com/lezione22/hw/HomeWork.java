package com.lezione22.hw;

public class HomeWork {

	public static void main(String[] args) {

		/**
		 * Creare un sistema di gestione Clienti
		 * 
		 * Ogni cliente � caratterizzato da:
		 * - Nome
		 * - Cognome
		 * - Numero di telefono
		 * - Codice Cliente
		 * 
		 * 1. Lettura dei dati su DB
		 * 2. Inserimento dei dati su DB
		 * 3. Esportazione dei record da DB a File
		 * 4. Eliminazione del record su DB
		 * 5. Modifica del record su DB
		 * 6. HARD: Codice Cliente assegnato in automatico dal sistema sotto formato "CLI001"...
		 * 
		 * DB -> JAVA CR -> ESPORTAZIONE FILE -> JAVA DU�
		 * 
		 * REVERSE:
		 * 1. Importa tutti i dati pulendo il database da Workbench
		 * 2. Importa tutti i dati pulendo il database da JAVA
		 * 3. HARD: Importa SOLO le righe dal CSV che non sono gi� presenti in DB (controlla il codice cliente)
		 * 
		 */
		
	}

}

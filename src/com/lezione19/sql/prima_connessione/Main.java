package com.lezione19.sql.prima_connessione;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Main {

	public static void main(String[] args) {

		
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setServerName("127.0.0.1");  //equivale a localhost
		dataSource.setPortNumber(3306);
		dataSource.setUser("root");
		dataSource.setPassword("toor");
		dataSource.setUseSSL(false);//settiamo in modo che possa comunicare anche in http senza ssl 
		dataSource.setDatabaseName("lez_19_rubrica");
		
		//SELECT
		try {

			ArrayList<Persona> elenco = new ArrayList<Persona>();
			
			Connection conn = dataSource.getConnection();
			
			//Spedisco la query mettendola in un pacchetto chiamato ps
			String query = "SELECT PersonaID, Nome, Cognome, Telefono FROM Persona;";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
			
			//Ricevo la query e la metto in un Result Set
			ResultSet risultato = ps.executeQuery();
			while(risultato.next()) {
				int var_id =  risultato.getInt(1);
				String var_nome = risultato.getString(2);
				String var_cognome = risultato.getString(3);
				String var_telefono = risultato.getString(4);
				
				Persona temp = new Persona();
				temp.setId(var_id);
				temp.setNome(var_nome);
				temp.setCognome(var_cognome);
				temp.setTelefono(var_telefono);
				elenco.add(temp);
			}
			
			conn.close();
			
			for(int i=0; i<elenco.size(); i++) {
				Persona temp_per = elenco.get(i);
				System.out.println(temp_per.toString());
			}
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		/*
		 * SELECT degli studenti all'interno di un database di studenti!
		 */
		
		//INSERT
		try {
			Connection conn = dataSource.getConnection();
			
			String query_inserimento = "INSERT INTO Persona(Nome, Cognome, Email, Telefono) VALUE (?, ?, ?, ?)";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, "Pastrocchio");
			ps.setString(2, "Cicinelli");
			ps.setString(3, "pastrocchio@ciao.com");
			ps.setString(4, "4785956143");
			
			ps.executeUpdate();								//Eseguo l'inserimento
			ResultSet risultato = ps.getGeneratedKeys();	//Prendo il risultato che � l'indice (AI) della riga appena creata
			risultato.next();
			
			int id_risultante = risultato.getInt(1);
			if(id_risultante > 0) {
				System.out.println("Inserimento effettuato con id: " + id_risultante);
			}
			else {
				System.out.println("Errore di inserimento!");
			}
			
			
			conn.close();
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		//DELETE
//		try {
//			Connection conn = dataSource.getConnection();
//			
//			String query_delete = "DELETE FROM Persona WHERE PersonaID = ?";
//			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_delete);
//			ps.setInt(1, 4);										//Elimino la Persona con ID 4
//			
//			int risultato_operazione = ps.executeUpdate();
//			if(risultato_operazione > 0) {
//				System.out.println("Riga eliminata!");
//			}
//			else {
//				System.out.println("Errore di eliminazione");
//			}
//			
//			
//		} catch (SQLException e) {
//			System.out.println(e.getMessage());
//		}
		
		//UPDATE
		try {
			Connection conn = dataSource.getConnection();
			
			String query_update = "UPDATE Persona SET "
					+ "Nome = ?,"
					+ "Cognome = ?,"
					+ "Telefono = ?"
					+ "WHERE PersonaID = ?";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_update);
			ps.setString(1, "Artabano");
			ps.setString(2, "Bernabeo");
			ps.setString(3, "789456123");
			ps.setInt(4, 9);
			
			int risultato_update = ps.executeUpdate();
			if(risultato_update > 0) {
				System.out.println("Riga modificata!");
			}
			else {
				System.out.println("Errore di modifica");
			}
			
			
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}

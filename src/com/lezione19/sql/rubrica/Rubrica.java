package com.lezione19.sql.rubrica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Rubrica {

	private ArrayList<Persona> elenco = new ArrayList<Persona>();
	private MysqlDataSource dataSource;
	
	
	Rubrica(){
		
		this.dataSource = new MysqlDataSource();
		this.dataSource.setServerName("127.0.0.1");  //equivale a localhost
		this.dataSource.setPortNumber(3306);
		this.dataSource.setUser("root");
		this.dataSource.setPassword("toor");
		this.dataSource.setUseSSL(false);//settiamo in modo che possa comunicare anche in http senza ssl 
		this.dataSource.setDatabaseName("lez_19_rubrica");
		
		this.select();
	}
	
	private void select() {
		
		try {
			
			this.elenco.clear();
			
			Connection conn = this.dataSource.getConnection();
			
			String query = "SELECT PersonaID, Nome, Cognome, Email, Telefono FROM Persona;";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
			
			ResultSet risultato = ps.executeQuery();
			while(risultato.next()) {
				int var_id =  risultato.getInt(1);
				String var_nome = risultato.getString(2);
				String var_cognome = risultato.getString(3);
				String var_email = risultato.getString(4);
				String var_telefono = risultato.getString(5);
				
				Persona temp = new Persona();
				temp.setId(var_id);
				temp.setNome(var_nome);
				temp.setCognome(var_cognome);
				temp.setEmail(var_email);
				temp.setTelefono(var_telefono);
				this.elenco.add(temp);
			}
			
			conn.close();
			
		} catch (Exception e) {
			System.out.println("Errore di reperimento dati!");
		}
		
	}
	
	public boolean inserisciNuovaPersona(String var_nome, String var_cognome, String var_email, String var_telefono) {
		Persona nuova = new Persona();
		nuova.setNome(var_nome);
		nuova.setCognome(var_cognome);
		nuova.setEmail(var_email);
		nuova.setTelefono(var_telefono);

		try {
			insert(nuova);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(nuova.getId() > 0) {
//			elenco.add(nuova);		//Poco sicuro rispetto ad una funzione di stampa che non ha la select prima della stampa!
			return true;
		}
		return false;
	}
	
	private Persona insert(Persona obj_persona) throws SQLException{
			Connection conn = dataSource.getConnection();
			
			String query_inserimento = "INSERT INTO Persona(Nome, Cognome, Email, Telefono) VALUE (?, ?, ?, ?)";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, obj_persona.getNome());
			ps.setString(2, obj_persona.getCognome());
			ps.setString(3, obj_persona.getEmail());
			ps.setString(4, obj_persona.getTelefono());
			
			ps.executeUpdate();								//Eseguo l'inserimento
			ResultSet risultato = ps.getGeneratedKeys();	//Prendo il risultato che � l'indice (AI) della riga appena creata
			risultato.next();
			
			int id_risultante = risultato.getInt(1);
			if(id_risultante == 0) {
				throw new SQLException();
			}
			obj_persona.setId(id_risultante);
			
			conn.close();
			return obj_persona;
	}
	
	public void stampaElenco() {
		this.select();
		
		for(int i=0; i<this.elenco.size(); i++) {
			Persona temp = this.elenco.get(i);
			System.out.println(temp.toString());
		}
	}
	
	/**
	 * Funzione di eliminazione di tutte le persone corrispondi a Nome e Cognome
	 * @param var_nome
	 * @param var_cognome
	 * @return	-1 se errore di eliminazione | 0 se non ho trovato nessuno | 1-n altrimenti
	 */
	public int eliminaPersona(String var_nome, String var_cognome) {
		this.select();
		int cont_eliminazioni = 0;
		
		for(int i=0; i<this.elenco.size(); i++) {
			Persona temp = this.elenco.get(i);
			
			if(temp.getNome().equals(var_nome) && temp.getCognome().equals(var_cognome)) {
				if(delete(temp.getId())) {
					cont_eliminazioni++;
				}
				else {
					return -1;
				}
			}
		}
		
		return cont_eliminazioni;
	}
	
	private boolean delete(int var_id) {
		try {
			Connection conn = dataSource.getConnection();
			
			String query_delete = "DELETE FROM Persona WHERE PersonaID = ?";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_delete);
			ps.setInt(1, var_id);										//Elimino la Persona con ID 4
			
			int risultato_operazione = ps.executeUpdate();
			if(risultato_operazione > 0) {
				return true;
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	private Persona findById(int var_id) {

		try {
			
			Connection conn = this.dataSource.getConnection();
			
			String query = "SELECT "
					+ "PersonaID, "
					+ "Nome, "
					+ "Cognome, "
					+ "Email, "
					+ "Telefono "
					+ "FROM Persona "
					+ "WHERE PersonaID = ?;";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
			ps.setInt(1, var_id);
			
			ResultSet risultato = ps.executeQuery();
			risultato.next();
			
			String var_nome = risultato.getString(2);
			String var_cognome = risultato.getString(3);
			String var_email = risultato.getString(4);
			String var_telefono = risultato.getString(5);
			
			Persona temp = new Persona();
			temp.setId(var_id);
			temp.setNome(var_nome);
			temp.setCognome(var_cognome);
			temp.setEmail(var_email);
			temp.setTelefono(var_telefono);
			
			conn.close();
			
			return temp;
			
		} catch (Exception e) {
			return null;
		}

	}
	
	public boolean modificaUtente(
			int var_id, 
			String var_nome, 
			String var_cognome, 
			String var_telefono, 
			String var_email) {
		Persona temp_persona = this.findById(var_id);
		
		if(temp_persona == null) {
			System.out.println("non ho trovato nulla");
			return false;
		}
		
		if(!var_nome.isEmpty())
			temp_persona.setNome(var_nome);
		if(!var_cognome.isEmpty())
			temp_persona.setCognome(var_cognome);
		if(!var_telefono.isEmpty())
			temp_persona.setTelefono(var_telefono);
		if(!var_email.isEmpty())
			temp_persona.setEmail(var_email);
		
		boolean esito_operazione = false;
		try {
			
			Connection conn = this.dataSource.getConnection();
			
			String query_update = "UPDATE Persona SET "
					+ "nome = ?, "
					+ "cognome = ?, "
					+ "email = ?,"
					+ "telefono = ?"
					+ "WHERE PersonaID = ?";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_update);
			ps.setString(1, temp_persona.getNome());
			ps.setString(2, temp_persona.getCognome());
			ps.setString(3, temp_persona.getEmail());
			ps.setString(4, temp_persona.getTelefono());
			ps.setInt(5, var_id);
					
			int righe_modificate = ps.executeUpdate();
			if(righe_modificate > 0) {
				esito_operazione = true;
			}
			
			conn.close();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			return esito_operazione;
		}
	
	}
	
	
}

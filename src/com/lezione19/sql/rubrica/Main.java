package com.lezione19.sql.rubrica;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Rubrica personale = new Rubrica();
//		personale.stampaElenco();
		
//		boolean risultato_inserimento = personale.inserisciNuovaPersona(
//				"Maria", "Vento", "vento@lol.com", "123456");
//	
//		if(risultato_inserimento) {
//			System.out.println("Ho effettuato l'inserimento");
//		}
//		else {
//			System.out.println("Errore di inserimento");
//		}
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		while(true) {
//			System.out.println("Istruzione I");
//			String istruzione = interceptor.nextLine();
//			if(istruzione.contentEquals("I")) {
//				personale.stampaElenco();
//			}
//		}
		
		/*
		 * DELETE: Nome e Cognome
		 * UPDATE...
		 */
		
//		int eliminazioni = personale.eliminaPersona("Mario", "Rossi");
//		
//
//		switch(eliminazioni) {
//		case 0:
//			System.out.println("Non ho trovato nessuno");
//			break;
//		case -1:
//			System.out.println("Errore di eliminazione sul DB");
//			break;
//		default:
//			System.out.println("Ho eliminato: " + eliminazioni + " persone!");
//		}
		
		//PER MOTIVI PRETTAMENTE DIDATTICI acedo al id dal main -.-'
		
		boolean modifica_effettuata = 
				personale.modificaUtente(11, "Undici", "", "", "");
	
		if(modifica_effettuata) {
			System.out.println("Tutto ok!");
		}
		else {
			System.out.println(";(");
		}
		
		/*
		 * Creare un piccolo sistem di gestione studenti,
		 * ogni studente sar� caratterizzato da:
		 * - Nome
		 * - Cognome
		 * - Telefono
		 * - Matricola <-------------------------
		 * 
		 * CRUD di studente, con particolare attenzione all'update che non deve ricevere ID dal main
		 * Es. Modifica tutti i dati di uno studente conoscendo la matricola!
		 * P.S.: La matricola non � modificabile ed univoca!
		 */
	}

}

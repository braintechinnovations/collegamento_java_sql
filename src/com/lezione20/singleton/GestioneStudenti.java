package com.lezione20.singleton;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

public class GestioneStudenti {

	private ArrayList<Studente> elenco_studenti = new ArrayList<Studente>();
	
	public boolean inserisciStudente(String var_nome, String var_matricola, String var_telefono) {
		Studente stud = new Studente();
		stud.setNominativo(var_nome);
		stud.setMatricola(var_matricola);
		stud.setTelefono(var_telefono);
		
		try {
			insert(stud);
			
			if(stud.getId() > 0)
				return true;
			
			return false;
		} catch (SQLException e) {
			return false;
		}
		

//		throw new SQLException();	//ATTENZIONE: Comunque termino la funzione con una Exception anche se il ramo ELSE della IF non esiste
	}
	
	private Studente insert(Studente stud) throws SQLException {
//		ConnettoreDB connettore = ConnettoreDB.getIstanza();
//		Connection connessione = connettore.getConnessione();
		
		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		String query_inserimento = "INSERT INTO Studente(Nominativo, Matricola, Telefono) VALUE (?, ?, ?)";
		PreparedStatement ps = (PreparedStatement)connessione.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);
		
		ps.setString(1, stud.getNominativo());
		ps.setString(2, stud.getMatricola());
		ps.setString(3, stud.getTelefono());
		
		ps.executeUpdate();								//Eseguo l'inserimento
		ResultSet risultato = ps.getGeneratedKeys();	//Prendo il risultato che � l'indice (AI) della riga appena creata
		risultato.next();
		
		int id_risultante = risultato.getInt(1);
		stud.setId(id_risultante);
		
		return stud;
	}
	
}

package com.lezione20.singleton;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {

	public static void main(String[] args) {

		GestioneStudenti segr_stud = new GestioneStudenti();
		segr_stud.inserisciStudente("Giovanni", "AB123456", "123456");
	
		GestioneEsami segr_esa = new GestioneEsami();
		try {
			segr_esa.inserisciEsame("Fisica", "Mecozzi", 6);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/*
	 * Creare un sistema di gestione prodotti.
	 * Ogni prodotto � contraddistinto da una o pi� categorie
	 */

}

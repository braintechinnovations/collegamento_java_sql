package com.lezione20.singleton;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

public class GestioneEsami {

	private ArrayList<Esame> elenco_esami = new ArrayList<Esame>();
	
	public boolean inserisciEsame(String var_titolo, String var_professore, int var_crediti) throws SQLException {
		Esame esa = new Esame();
		esa.setTitolo(var_titolo);
		esa.setProfessore(var_professore);
		esa.setCrediti(var_crediti);
		
		insert(esa);
		if(esa.getId() > 0)
			return true;
		
		throw new SQLException();
	}
	
	private void insert(Esame esa) throws SQLException{
		
		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		
		String query_inserimento = "INSERT INTO Esame(Titolo, Professore, Crediti) VALUE (?, ?, ?)";
		PreparedStatement ps = (PreparedStatement)connessione.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);
		
		ps.setString(1, esa.getTitolo());
		ps.setString(2, esa.getProfessore());
		ps.setInt(3, esa.getCrediti());
		
		ps.executeUpdate();								//Eseguo l'inserimento
		ResultSet risultato = ps.getGeneratedKeys();	//Prendo il risultato che � l'indice (AI) della riga appena creata
		risultato.next();
		
		int id_risultante = risultato.getInt(1);
		esa.setId(id_risultante);
		
	}
	
}

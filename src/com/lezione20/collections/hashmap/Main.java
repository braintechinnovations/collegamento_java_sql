package com.lezione20.collections.hashmap;

import java.util.HashMap;

public class Main {

	public static void main(String[] args) {

		//HashMap che si comporta come un ArrayList (didattico)
//		HashMap<Integer, String> hm = new HashMap<Integer, String>();
//		
//		hm.put(0, "Giovanni");
//		hm.put(1, "Mario");
//		hm.put(2, "Valeria");
//		
//		System.out.println(hm.get(2));
		
		HashMap<String, String> hm = new HashMap<String, String>();
		
		hm.put("persona_1", "Giovanni");
		hm.put("persona_2", "Mario");
		hm.put("persona_3", "Valeria");
		
		System.out.println(hm.get("persona_1"));
		
		//Esiste un foreach per la HashMap sia per le chiavi che i valori?
		
	}

}

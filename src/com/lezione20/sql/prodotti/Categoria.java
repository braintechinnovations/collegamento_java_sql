package com.lezione20.sql.prodotti;

import java.util.ArrayList;

public class Categoria {

	private int cat_id;
	private String nome;
	private String descrzione;
	private ArrayList<Prodotto> elenco_prodotti = new ArrayList<Prodotto>();
	
	public int getCat_id() {
		return cat_id;
	}
	public void setCat_id(int cat_id) {
		this.cat_id = cat_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrzione() {
		return descrzione;
	}
	public void setDescrzione(String descrzione) {
		this.descrzione = descrzione;
	}
	@Override
	public String toString() {
		return "Categoria [cat_id=" + cat_id + ", nome=" + nome + ", descrzione=" + descrzione + ", elenco_prodotti="
				+ elenco_prodotti + "]";
	}
	
	
	
}

package com.lezione20.sql.prodotti;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class GestoreMagazzino {
	
	public void dettaglioProdotto(String var_nome, String var_codi) {

		try {
			HashMap<String, String> parametri = new HashMap<String, String>();
			parametri.put("nome", var_nome);
			parametri.put("codice", var_codi);
			
			ArrayList<Prodotto> ris = search(parametri);
			
			for(int i=0; i<ris.size(); i++) {
				System.out.println(ris.get(i).toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private ArrayList<Prodotto> search(HashMap<String, String> var_parametri) throws SQLException{
		Connection conn = Connettore.getInstance().getConnection();
		
		String sql_ricerca = "SELECT "
								+ "ProdottoID, "
								+ "codice, "
								+ "nome, "
								+ "prezzo "
								+ "FROM Prodotto "
								+ "WHERE 1=1 ";
		
//		int contatore_variabili = 0;
		if(!var_parametri.get("nome").isEmpty()) {
			sql_ricerca += "AND nome LIKE ? ";
//			contatore_variabili++;
		}
		if(!var_parametri.get("codice").isEmpty()) {
			sql_ricerca += "AND codice LIKE ? ";
//			contatore_variabili++;
		}
		
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(sql_ricerca);
		
		if(!var_parametri.get("nome").isEmpty() && var_parametri.get("codice").isEmpty()) 
			ps.setString(1, "%" + var_parametri.get("nome") + "%");
		else if(!var_parametri.get("codice").isEmpty() && var_parametri.get("nome").isEmpty()) 
			ps.setString(1, "%" + var_parametri.get("codice") + "%");
		else if(!var_parametri.get("nome").isEmpty() && !var_parametri.get("codice").isEmpty()) {
			ps.setString(1, "%" + var_parametri.get("nome") + "%");
			ps.setString(2, "%" + var_parametri.get("codice") + "%");
		}
		
		ResultSet risultato = ps.executeQuery();
		
		ArrayList<Prodotto> elenco_risultati = new ArrayList<Prodotto>();
		while(risultato.next()) {
			Prodotto temp = new Prodotto();
			temp.setPro_id(risultato.getInt(1));
			temp.setCodice(risultato.getString(2));
			temp.setNome(risultato.getString(3));
			temp.setPrezzo(risultato.getFloat(4));
			temp.setCategorie_prod(getCategorieByProdottoId(risultato.getInt(1)));
			
			elenco_risultati.add(temp);
		}
		
		return elenco_risultati;
	}
	
	public ArrayList<Categoria> getCategorieByProdottoId(int var_prod_id) throws SQLException{
		
		Connection conn = Connettore.getInstance().getConnection();

		String query_ricerca = "SELECT prodotto_id, CategoriaID, nome, descrizione" + 
				"	FROM Prodotto_Categoria" + 
				"    JOIN Categoria ON Prodotto_Categoria.categoria_id = Categoria.CategoriaID" + 
				"    WHERE prodotto_id = ?";
		
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_ricerca);
		ps.setInt(1, var_prod_id);
		
		ResultSet risultato = ps.executeQuery();
		
		ArrayList<Categoria> elenco_risultati = new ArrayList<Categoria>();
		while(risultato.next()) {
			Categoria temp = new Categoria();
			temp.setCat_id(risultato.getInt(2));
			temp.setNome(risultato.getString(3));
			temp.setDescrzione(risultato.getString(4));
			
			elenco_risultati.add(temp);
		}
		
		return elenco_risultati;
	}
}

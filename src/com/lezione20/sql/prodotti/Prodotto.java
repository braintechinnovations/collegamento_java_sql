package com.lezione20.sql.prodotti;

import java.util.ArrayList;

public class Prodotto {

	private int pro_id;
	private String codice;
	private String nome;
	private float prezzo;
	private ArrayList<Categoria> categorie_prod = new ArrayList<Categoria>();
	
	public int getPro_id() {
		return pro_id;
	}
	public void setPro_id(int pro_id) {
		this.pro_id = pro_id;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	public ArrayList<Categoria> getCategorie_prod() {
		return categorie_prod;
	}
	public void setCategorie_prod(ArrayList<Categoria> categorie_prod) {
		this.categorie_prod = categorie_prod;
	}
	
	@Override
	public String toString() {
		return "Prodotto [pro_id=" + pro_id + ", codice=" + codice + ", nome=" + nome + ", prezzo=" + prezzo
				+ ", categorie_prod=" + categorie_prod + "]";
	}
	
	
}
